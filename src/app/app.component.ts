import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'My App!!';

  a: number;
  b: number;
  c: number;

  fruits: any = ['Apple', 'Banana', 'Mango'];
  users: any = [
    {
      "id": 1,
      "fullname": "สถิตย์ เรียนพิศ",
      "username": "admin"
    },
    {
      "id": 3,
      "fullname": "Satit Rianpit",
      "username": "xxxx"
    }
  ];

  constructor() { }

  addNumber() {
    this.c = +this.a + +this.b;
  }
}
