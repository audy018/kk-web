import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    @Inject('API_URL') private apiUrl: string,
    private httpClient: HttpClient) { }

  async getUsers(token: string) {
    let url = this.apiUrl + '/users/detail?token=' + token;
    return await this.httpClient.get(url).toPromise();
  }

}
